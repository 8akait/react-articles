
import axios from 'axios';

import {ArticleDataType} from '../components/articles/articles.screen';

const top_stories_url = "https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty";

const GetTopStories = async () => {
    try {
        return await axios.get(top_stories_url);
    }catch(error){
        console.log(error)
    }
}

const GetStoryData = async (id: string): Promise<ArticleDataType | undefined> => {
    try {
        const response:any =  await axios.get(`https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`);
        if(response && response.data){
            return Promise.resolve(response.data);
        } else {
            return Promise.resolve(undefined);
        }
            
    }catch(error){
        return Promise.reject(error)
    }
}

export {
    GetTopStories,
    GetStoryData
}