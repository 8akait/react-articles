import React, { Component } from 'react';

import ArticleScreen, {ArticleDataType} from './components/articles/articles.screen';
import Loader from 'react-loader-spinner';

import './App.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";

import * as requestConfig from './configs/request.config.json';
import { GetTopStories, GetStoryData } from './configs/fetchData';
import _ from 'lodash';

const TOTAL_DATA_COUNT = requestConfig.totalRows;
const DURATION = 5 * 60 *1000;
// const DURATION = 5 *1000;

interface State {
  articlesData: ArticleDataType[];
}
interface Props {}

class App extends Component<Props, State> {
  private timer: any;
  constructor(props: Props){
    super(props);
    this.state = {
      articlesData: [],
    }
    this.fetchArticleData = this.fetchArticleData.bind(this);
  }
  async componentDidMount() {
      await this.fetchArticleData()
      this.timer = setInterval(this.fetchArticleData, DURATION);
  }
  componentWillUnmount() {
    clearInterval(this.timer);
  }

  updateStories(result: ArticleDataType) {
    this.setState((prevState: State) => {
      const cummulative = [...prevState.articlesData, result]
      const all = cummulative.sort((a: ArticleDataType, b: ArticleDataType) => b.time - a.time)
      const allunique = _.uniqBy(all, function (e) {
        return e.id;
      });
      const top20 = allunique.slice(0, TOTAL_DATA_COUNT);
      return {
        articlesData: top20
      }
    })
  }
  async fetchArticleData(){
    try {
      const response: any = await GetTopStories();
      const {data} = response;
      const stories = data.slice(0, TOTAL_DATA_COUNT);
      stories.map(async (id: string) => {
        const result: ArticleDataType | undefined = await GetStoryData(id);
        if(result){
          this.updateStories(result);
        }
      });
    }catch(error){
      console.log(error);
    }
  }
  render() {
    const {articlesData} = this.state;
    return (
      <div className="App">
        { articlesData.length > 0 ? 
            <ArticleScreen 
              articlesData={articlesData}
              />
              : <Loader
                  type="Puff"
                  color="#00BFFF"
                  height={100}
                  width={100}
                />
        }
      </div>
    );
  }
}

export default App;
