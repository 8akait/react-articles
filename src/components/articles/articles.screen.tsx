import React, {Component} from 'react';
import './articles.screen.css';
import { DropdownButton, Dropdown } from 'react-bootstrap';

export interface ArticleDataType {
    by: string,
    descendants : number,
    id : string,
    kids : any[],
    score : number,
    time : number,
    title : string,
    type : string,
    url : string
}

export interface Props {
    articlesData: ArticleDataType[]
}
type SortByType = 'score' | 'title' | 'author' | undefined;

interface State {
    totalPages: number;
    totalRowsPerPage: number,
    screenWidth: number,
    pageNumber: number,
    sortBy: SortByType,
    articlesData: ArticleDataType[],
    setSortBy: (articlesData: ArticleDataType[],title: SortByType, isSetState: boolean) => void
}

class ArticleScreen extends Component<Props, State> {
    constructor(props: Props){
        super(props);
        this.state = {
            totalRowsPerPage: 5,
            screenWidth: window.innerWidth,
            pageNumber: 0,
            totalPages: 1,
            sortBy: undefined,
            articlesData: [],
            setSortBy: this.setSortBy
        }

        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.renderArticleData = this.renderArticleData.bind(this);
        this.setTotalRowPerPage = this.setTotalRowPerPage.bind(this);
        this.setSortBy = this.setSortBy.bind(this);
    }
    componentDidMount() {
        window.addEventListener('resize', this.updateWindowDimensions);
      }
      
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }
      
    updateWindowDimensions() {
        this.setState({ screenWidth: window.innerWidth });
    }

    static getDerivedStateFromProps(nextProps: Props, prevState: State){
        if (prevState.articlesData !== nextProps.articlesData) {
            return {
                articlesData: prevState.setSortBy(nextProps.articlesData, prevState.sortBy, false),
            };
          }
          return null;
    }
    getTotalRowsPerPage(pageNumber: number) {
        const {totalRowsPerPage, articlesData} = this.state;
        const upperLimit = (pageNumber + 1) * totalRowsPerPage;
        const lowerLimit = pageNumber * totalRowsPerPage;

        const length = articlesData.length;
        return upperLimit < length ? totalRowsPerPage: (length - lowerLimit);
    }
    onClickPageNumber(pageNumber: number) {
        this.setState({
            pageNumber: pageNumber,
            totalRowsPerPage: this.getTotalRowsPerPage(pageNumber)
        })
    }
    renderPageNumberPanel(totalPages: number) {
        const numbers:number[] = []
        for(let i=0; i < totalPages; i++){
            numbers.push(i)
        }
        return (
            <div className="pagePanel">
                {numbers.map((num: number) => {
                    return (
                        <div key={'pageNum'+num} className="pageNumberStyle" onClick={() => this.onClickPageNumber(num)}>
                            {num + 1}
                        </div>  
                    )
                })}
            </div>
        )
    }

    renderArticleData(index: number) {
        const {articlesData} = this.state;
        const {totalRowsPerPage, pageNumber} = this.state;

        const upperLimit = (pageNumber + 1) * totalRowsPerPage;
        const lowerLimit = pageNumber * totalRowsPerPage;
        const selectedArticles = articlesData ? articlesData.slice(lowerLimit, upperLimit): undefined;

        if(selectedArticles && selectedArticles[index]) {
            const data: ArticleDataType = selectedArticles[index];
            const {id, score , title, by} = data;
            return (
                <div className="Card"
                    key={id}
                    >
                        <div className="Row">
                            <div className="Header">Title:</div>
                            <div className="Title">{title}</div>
                        </div>
                        <div className="Row">
                            <div className="Header">score:</div>
                            <div className="Title">{score}</div>
                        </div>
                        <div className="Row">
                            <div className="Header">auther:</div>
                            <div className="Title">{by}</div>
                        </div>
                        

                </div>
            )
        }
    } 

    setTotalRowPerPage(total: number) {
        this.setState({
            totalRowsPerPage: total
        })
    }
    setSortBy(articlesData: ArticleDataType[],title: SortByType, isSetState: boolean){
        let data = [];
        const compare = ( a: ArticleDataType, b: ArticleDataType, key: SortByType) => {
            const k = key==='title' ? 'title' : 'by';
            if (a[k] < b[k]){
              return -1;
            }
            if (a[k] > b[k]){
              return 1;
            }
            return 0;
        }
        switch(title){
            case 'score': {
                data = articlesData.sort(function(a: ArticleDataType, b: ArticleDataType){return a.score - b.score})
                break;
            }
            case 'title': {
                data = articlesData.sort(function(a: ArticleDataType, b: ArticleDataType){return compare(a,b, 'title')})
                break;
            }
            case 'author': {
                data = articlesData.sort(function(a: ArticleDataType, b: ArticleDataType){return compare(a,b, 'author')})
                break;
            }
            default: {
                data = articlesData;
            }
        }
        if (isSetState){
            this.setState({
                sortBy: title,
                articlesData: data
            })
        } else {
            return data;
        }
    }
    renderSearchBar() {
        const {articlesData} = this.state;
        return (
            <div className="SearchBarContainer">
                <DropdownButton id="dropdown-basic-button" title="Articles per page" className="SearchDropdown">
                    <Dropdown.Item><div onClick={() => this.setTotalRowPerPage(5)}>5</div></Dropdown.Item>
                    <Dropdown.Item><div onClick={() => this.setTotalRowPerPage(10)}>10</div></Dropdown.Item>
                    <Dropdown.Item><div onClick={() => this.setTotalRowPerPage(20)}>20</div></Dropdown.Item>
                </DropdownButton>
                <DropdownButton id="dropdown-basic-button" title="Sort By" className="SearchDropdown">
                    <Dropdown.Item><div onClick={() => this.setSortBy(articlesData, 'score', true)}>score</div></Dropdown.Item>
                    <Dropdown.Item><div onClick={() => this.setSortBy(articlesData, 'title', true)}>title</div></Dropdown.Item>
                    <Dropdown.Item><div onClick={() => this.setSortBy(articlesData, 'author', true)}>author</div></Dropdown.Item>
                </DropdownButton>
            </div>
        )
    }
    render() {
        const {articlesData} = this.state;
        const length = articlesData.length;
        const totalPages = Math.ceil(length / this.state.totalRowsPerPage);
        return (
            <div className="ArticleContainer">
                {this.renderSearchBar()}
                {articlesData.map((article: ArticleDataType, index: number) => this.renderArticleData(index))}
                {this.renderPageNumberPanel(totalPages)}
            </div>
        )
    }
}

export default ArticleScreen
